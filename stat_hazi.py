import numpy as np
import scipy as sp
import pandas as pd
import statsmodels
# Different types of data----

# numerical vector----
Age = [46,42,59,78,13,25]
Ages = [46,42,59,78,13,25]
# type of data
type(Age)
# data structure
str(Age) ?
# refer to the 3rd element of the vector
Age[2]
# basic variables
# sum
sum(Age)
sum(Age[0:3])
# mean
np.mean(Age)
# median
np.median(Age)
# standard deviation
np.std(Age)
# description
#Age.describe ?
# see the help page
#help(median) ?

# matrix----
Ages_half=np.divide(Ages,2)
Ages_double=np.multiply(Ages,2)

# you can add names to the variables
Names = np.array(["Joe","Biden","Donald","Trump","Jane","Jessica"])

# matrix----
Ages_m = np.dstack((Names, Ages, Ages_half, Ages_double))

Age = pd.DataFrame(dict(age=[46,42,59,78,13,25], name=["Joe","Biden","Donald","Trump","Jane","Jessica"]))
Age_half=Age.copy()
Age_half['age']=Age_half['age'].div(2)
Age_double=Age.copy()
Age_double['age']=Age_double['age']*2
Age_m=pd.concat([Age,Age_half,Age_double],axis=1)
Age_m.index=["Joe","Biden","Donald","Trump","Jane","Jessica"]
Age_m=Age_m.drop(['name'],axis=1)
Age_m.columns=['age','age_half','age_double']
type(Age_m)
Age_m.shape
Age_m.describe
print(Age_m)
# see the dimensions of the matrix
Age_m.shape
# refer to one elemen of a matrix (row, column order)
Age_m.loc['Jessica', 'age':'age_double']
# refer to a single variable
Age_m['age']
list(Age_m.columns)
list(Age_m.index)
print(Age_m)

# create a new variable (column)
Age_m['gender'] = ['man','man','man','man','woman','woman']
type(Age_m['gender'])

# filter a data frame based on the values of a given variable----
# logical operators
# < 	less than
# <= 	less than or equal to
# > 	greater than
# >= 	greater than or equal to
# == 	exactly equal to
# != 	not equal to
# !x 	Not x
# x | y 	x OR y
# x & y 	x AND y
# isTRUE(x) 	test if X is TRUE 

# men
Age_df_men=Age_m.copy(deep=True)
Age_df_men=Age_df_men[Age_df_men.gender=='man']
Age_df_men
# not men
Age_df_nomen=Age_m.copy(deep=True)
Age_df_nomen=Age_df_nomen[Age_df_nomen.gender!='man']
Age_df_nomen
# younger than 20
Age_df_under20=Age_m.copy(deep=True)
Age_df_under20=Age_df_under20[Age_df_under20.age<20]
Age_df_under20
# man, who are older than 50
Age_df_men50=Age_m.copy(deep=True)
Age_df_men50=Age_df_men50[(Age_df_men50.gender=='man') & (Age_df_men50.age>50)]
Age_df_men50

# Basic plots----
from pandas.plotting import scatter_matrix
scatter_matrix(Age_m)
from matplotlib import pyplot as plt
plt.plot(Age_m.gender.value_counts())
plt.plot(Age_m.age)
plt.plot(Age_m.age,Age_m.age_half)
import seaborn as sns
sns.boxplot(x='gender', y='age',data=Age_m)

# list----
Age_list=[Age_m.age,Age_m.age_double,Age_m.age_half]
type(Age_list)
#Age_list.shape ?
# refer to a part of the list
Age_list[0]
Age_list[0:2]
#summary(Age_list) ?

# Working with packages----
# Set working directory, data export and import----
Age_m.to_csv(path_or_buf=r'C:\Users\csoda\OneDrive\Dokumentumok\pythonora\Age_m.csv', sep='\t', header=True, index=True)

# Use a new data set for the already give functions----
# Source of data:
# B�nfi R, Pohner Zs, Szab� A, Herczeg G, Kov�cs GM, Nagy A, M�rialigeti K, Vajna B. 2021. Succession and potential role of bacterial communities during Pleurotus ostreatus production, FEMS Microbiology Ecology, 97, fiab125, DOI: 10.1093/femsec/fiab125

Pleurotus = pd.read_table(filepath_or_buffer="/mnt/c/Users/csoda/OneDrive/Dokumentumok/pythonora/Pleurotus_model_system_original_enzyme_activities.txt", header=0, index_col=0)
print(Pleurotus)
type(Pleurotus)
Pleurotus.shape
def front(self, n):
    return self.iloc[:, :n]

def back(self, n):
    return self.iloc[:, :-n]

pd.DataFrame.front = front
pd.DataFrame.back = back
Pleurotus.front(n=7)
Pleurotus.back(n=7)
Pleurotus.describe
plt.scatter(Pleurotus.index, Pleurotus.Endocellulase)
sns.boxplot(x='Region', y='Endocellulase',data=Pleurotus)

# Remove control samples
Pleurotus_col=Pleurotus.copy(deep=True)
Pleurotus_col=Pleurotus_col[(Pleurotus.Region!="3_nc") & (Pleurotus.Region!="1_neg_cont_4w") & (Pleurotus.Region!="2_neg_cont_8w")]
sns.boxplot(x='Region', y='Endocellulase',data=Pleurotus_col)

# Standardization----
# Standardization with range: (x-minimum)/(maximum-minimum)
Pleurotus_col_range=Pleurotus_col.copy(deep=True)
Pleurotus_col_range['Endocellulase'] = (Pleurotus_col_range['Endocellulase']-Pleurotus_col_range['Endocellulase'].min())/(Pleurotus_col_range['Endocellulase'].max()-Pleurotus_col_range['Endocellulase'].min())
Pleurotus_col_range['Endocellulase']

# apply: there is no need to use rescale for every variable separately
Pleurotus_col_range.iloc[:,5:13] = (Pleurotus_col_range.iloc[:,5:13]-Pleurotus_col_range.iloc[:,5:13].min())/(Pleurotus_col_range.iloc[:,5:13].max()-Pleurotus_col_range.iloc[:,5:13].min())
sns.boxplot(x='Region', y='Endocellulase',data=Pleurotus_col_range)
sns.boxplot(x='Region', y='Endocellulase',data=Pleurotus_col)

# Log10 transform
Pleurotus_col_lg=Pleurotus_col.copy(deep=True)
Pleurotus_col_lg.iloc[:,5:13] = np.log10(Pleurotus_col_lg.iloc[:,5:13])
# or we create a new variable in the original data frame
Pleurotus_col['Endocellulase_log'] = np.log10(Pleurotus_col['Endocellulase'])
plt.scatter(Pleurotus_col['Endocellulase'], Pleurotus_col['Endocellulase_log'])

# Standardization: extract group mean and divide by group standard deviation: scale function----
Endocell_scale = (Pleurotus_col['Endocellulase'] - Pleurotus_col['Endocellulase'].mean())/Pleurotus_col['Endocellulase'].std()
Endocell_scale.describe
plt.scatter(Endocell_scale, Pleurotus_col['Endocellulase'])

# write own function----
# do scaling with own function
import statistics as stat
def Stand_scale(x):
    scaled=[]
    for num in x:
        scaled.append((num-stat.mean(x))/stat.stdev(x))
    return scaled

Stand_scale(Pleurotus_col['Endocellulase'])

# if I would like to perform scale for every group at once
Endocell_group_scale2 = Pleurotus_col.groupby('Region').transform(lambda x: (x - x.mean()) / x.std())
Endocell_group_scale2 = Endocell_group_scale2['Endocellulase']
Endocell_group_scale2.shape
sns.boxplot(x = Pleurotus['Region'], y = Endocell_group_scale2)

# if I would like to perform scale for every group and for every variable at once
Pleurotus_col_st = Pleurotus_col.groupby('Region').transform(lambda x: (x - x.mean()) / x.std())
Pleurotus_col_st = Pleurotus_col_st.drop(['Contamination', 'Speed'], axis=1)

# Merge two data set----
# use merge for original and log10 transformed dataset
# it is especially useful if the order of the samples are not the same
# range samples according to one variable
Pleurotus_col_lg=Pleurotus_col_lg.sort_values(by=['Reg_Tr'])
Pleurotus_col_m=Pleurotus_col.join(Pleurotus_col_lg, lsuffix='.x', rsuffix='.y')
Pleurotus_col_m=Pleurotus_col_m.sort_values(by=['Reg_Tr.x'])

# if, ifelse (loop)----
x = -5
if x > 5:
    print("Non-negative number")
else:
    print("Negative number")

# Introduce new variable, if Treatment=A, AA, AW --> "Higher cont"; otherwise "Lower cont"
Pleurotus_col['Treatment_type'] = 'NA'
Pleurotus_col['Treatment_type'] = ['Higher_cont' if x =="0_A" or x =="1_AW" or x =="2_AA" else 'Lower_cont' for x in Pleurotus_col['Treatment']]

# Convert data frame to binary
Pleurotus_col_st_01 = Pleurotus_col_st.copy(deep=True)
Pleurotus_col_st_01['Endocellulase'] = np.where(Pleurotus_col_st_01['Endocellulase']>0,1,0)

# Search for extreme values in a data frame
Pleurotus_col_st2 = []
for i in Pleurotus_col_st['Endocellulase']:
    if i < -1:
        Pleurotus_col_st2.append('Low')
    elif i > -1 and i < 1:
        Pleurotus_col_st2.append("Normal")
    else:
        Pleurotus_col_st2.append("High")

Pleurotus_col_st2

# Replace, find one character from a string----
Treatment_type_hun = Pleurotus_col['Treatment_type'].copy(deep=True)
Treatment_type_hun = ['Magas_fertőzés' if x == 'Higher_cont' else 'Alacsony_fertőzés' for x in Treatment_type_hun]

# Get 3rd character of each sample name
Treatment_type_hun_code = []
for x in Treatment_type_hun:
    Treatment_type_hun_code.append(x[0])

Treatment_type_hun_code

# Correlation, t-test, f-test----
# graphical view
scatter_matrix(Pleurotus_col[['Endocellulase','Endoxylanase','B_glucosidase','Cellobiohydrolase']])

# Pearson and spearman correlation
from scipy.stats import pearsonr
pearcorr = pearsonr(Pleurotus_col['Endocellulase'],Pleurotus_col['Endoxylanase'])
print(pearcorr)
from scipy.stats import spearmanr
spearcorr = spearmanr(Pleurotus_col['Endocellulase'],Pleurotus_col['Endoxylanase'])
print(spearcorr)

if pearcorr[1] < 0.05:
    print('Significant')
else:
    print('Not significant')

if spearcorr[1] < 0.05:
    print('Significant')
else:
    print('Not significant')

# t-test
from scipy.stats import ttest_ind
ttest = ttest_ind(Pleurotus_col['Endocellulase'],Pleurotus_col['Endoxylanase'])
print(ttest)

if ttest[1] < 0.05:
    print('Significant')
else:
    print('Not significant')

# F-test
# test for normal distribution
from scipy.stats import shapiro
shapiro_test_1 = shapiro(Pleurotus_col['Endocellulase'])
print(shapiro_test_1)
plt.hist(Pleurotus_col['Endocellulase'])

shapiro_test_2 = shapiro(Pleurotus_col['Endoxylanase'])
print(shapiro_test_2)
plt.hist(Pleurotus_col['Endoxylanase'])

from scipy.stats import f_oneway
f_test = f_oneway(Pleurotus_col['Endocellulase'],Pleurotus_col['Endoxylanase'])
print(f_test)
if f_test[1] < 0.05:
    print('Significant')
else:
    print('Not significant')
